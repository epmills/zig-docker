#!/bin/bash
#
# NAME
#   master.sh
# SYNOPSIS
#   Build a Docker image with Zig binary and library then pushes to Gitlab
#   for use with other Zig-related projects.
# DESCRIPTION
#   Uses a build of the master branch of Zig.
#   Should be updated as new version of Zig are released or to get a more
#   recent version of master.

REGISTRY=registry.gitlab.com/epmills
PROJECT=zig-docker
TAG_NOW="$(date +%Y%m%d%H%M)"
TAG_VERSION="master"

docker login registry.gitlab.com

docker build --no-cache -t $REGISTRY/$PROJECT:$TAG_NOW .
docker push $REGISTRY/$PROJECT:$TAG_NOW
docker tag $REGISTRY/$PROJECT:$TAG_NOW $REGISTRY/$PROJECT:$TAG_VERSION
docker push $REGISTRY/$PROJECT:$TAG_VERSION
