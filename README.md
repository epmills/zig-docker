# epmills/zig-docker

A docker image that can be used by other projects to build code written in the Zig programming language.

See https://ziglang.org for more information.

## Using a Pre-Built Docker Image

A Docker image that includes a version of Zig from the master branch is available
via:

```sh
docker pull registry.gitlab.com/epmills/zig-docker:master
```

## Roll Your Own

Fork this repository and update the `REPOSITORY` and `PROJECT` values in the
[master.sh] script to suit your needs.

The script tags the Docker image with the current timestamp and `master` so you
don't have to hit a moving (tag) target.

For bleeding-edge Zig functionality, update the [Dockerfile], changing the
`ZIG_BUILD` and `ZIG_BUILD_SHA256` values as shown in the [Releases] page of
the [ziglang web page].

[master.sh]: master.sh
[Dockerfile]: Dockerfile
[Releases]: https://ziglang.org/download/#release-master
[ziglang web page]: https://ziglang.org
