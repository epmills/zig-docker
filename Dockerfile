FROM alpine:3.11 AS build

LABEL maintainer="epmills <epmills@gitlab.com"

ENV ZIG_BUILD zig-linux-x86_64-0.5.0+e491b2f5a
ENV ZIG_BUILD_SHA256 1278b977d1036dee08e89092af5942dac63521f802f6b902484d03398ce1c274
ENV ZIG_DL_DIR=builds

RUN wget --quiet https://ziglang.org/${ZIG_DL_DIR}/$ZIG_BUILD.tar.xz \
    && echo "$ZIG_BUILD_SHA256  $ZIG_BUILD.tar.xz" | sha256sum -c - \
    && tar -xf $ZIG_BUILD.tar.xz \
    && mv $ZIG_BUILD zig-dist \
    && rm $ZIG_BUILD.tar.xz

FROM alpine:3.11 AS dist
COPY --from=build /zig-dist /z
